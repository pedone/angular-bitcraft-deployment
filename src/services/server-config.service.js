/*global angular*/
'use strict';

angular.
    module('bitcraft-server-config').
    factory('ServerConfig', [
        '$http', '$log', '$q',
        function ($http, $log, $q) {
            /** @returns {Promise} */
            function getCurrentConfig() {
                var deferred = $q.defer();
                $http.get('./rest/getCurrentConfig').then(function successCallback(res) {
                    deferred.resolve(res.data);
                }, function errorCallback(resp) {
                    $log.error('Failed to retrieve the current config (' + resp.statusText + ')');
                    deferred.reject(resp);
                });
                return deferred.promise;
            }

            return {
                getCurrentConfig: getCurrentConfig
            };
        }
    ]);
