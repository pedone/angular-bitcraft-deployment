#!/usr/bin/env bash
if [ "$#" -ne  "2" ]; then
    echo "Usage: ./install_sentinel.sh localAddress rootDirectory"
    exit 1
fi

function checkResult() {
    retCode=$?
    if [ $retCode -ne 0 ]; then
        exit $retCode
    fi
}

ADDRESS=$1
ROOT_DIRECTORY=$2

#uninstall existing version
UNINSTALL_COMMAND="$(dirname ${BASH_SOURCE[0]})/uninstall_sentinel.sh"
$UNINSTALL_COMMAND $ADDRESS $ROOT_DIRECTORY
checkResult

SENTINEL_DIRECTORY="$ROOT_DIRECTORY/sentinel"
SOURCE_ARCHIVE_DIRECTORY='../distributionCache'

# get last available version number
VERSION=`ls $SOURCE_ARCHIVE_DIRECTORY | grep "sentinel.*" | sort -V | tail -n 1 | sed 's|sentinel.\([0-9]\+.[0-9]\+.[0-9]\+\).tar.gz|\1|'`
TAR_FILE="sentinel-$VERSION.tar.gz"

if [ ! -d $SOURCE_ARCHIVE_DIRECTORY ] || [ -z "$VERSION" ]
then
    echo "No sentinel archive found. Upload sentinel archive and try again" >&2
    exit 1
fi

ssh -o ConnectTimeout=5 $ADDRESS "test -e $TAR_FILE"
RESULT=$?
if [ $RESULT -eq 255 ]; then
    exit 255
fi

TAR_FILE_EXISTS_REMOTE=$RESULT
if [ $TAR_FILE_EXISTS_REMOTE -ne 0 ]; then
    # we need to deploy a new version
    # copy everything to the distant server
    scp "$SOURCE_ARCHIVE_DIRECTORY/$TAR_FILE" $ADDRESS:~
    checkResult

    echo "$TAR_FILE sent to $ADDRESS"
fi

echo "installing new sentinel version $VERSION to host $ADDRESS"

#unpack archive
ssh $ADDRESS "tar -xzf $TAR_FILE"

if [ $TAR_FILE_EXISTS_REMOTE -ne 0 ]; then
    # copy archive into distribution cache directory
    ssh $ADDRESS "mv $TAR_FILE $ROOT_DIRECTORY/distributionCache"
fi

#npm install sentinel
ssh $ADDRESS "cd $SENTINEL_DIRECTORY; npm install"
checkResult

#start sentinel
ssh $ADDRESS "source .bash_profile; cd $SENTINEL_DIRECTORY; forever start -a --uid sentinel-$ADDRESS --killSignal=SIGTERM index.js"
checkResult