#!/usr/bin/env bash
if [ "$#" -ne  "4" ]; then
    echo "Usage: ./install_http.sh localAddress file name rootDirectory"
    exit 1
fi

function checkResult() {
    retCode=$?
    if [ $retCode -ne 0 ]; then
        exit $retCode
    fi
}

ADDRESS=$1
SOURCE_FILE=$2
NAME=$3
ROOT_DIRECTORY=$4

#uninstall existing version
UNINSTALL_COMMAND="$(dirname ${BASH_SOURCE[0]})/uninstall_http.sh"
$UNINSTALL_COMMAND $ADDRESS $ROOT_DIRECTORY
checkResult

HTTP_DIRECTORY="$ROOT_DIRECTORY/http"
DEPLOYMENT_DIRECTORY="$ROOT_DIRECTORY/deployment"
TAR_FILE=$NAME

ssh -o ConnectTimeout=5 $ADDRESS "test -e $TAR_FILE"
RESULT=$?
if [ $RESULT -eq 255 ]; then
    exit 255
fi

TAR_FILE_EXISTS_REMOTE=$RESULT
if [ $TAR_FILE_EXISTS_REMOTE -ne 0 ]; then
    #send archive
    scp $SOURCE_FILE $ADDRESS:~/$TAR_FILE
    checkResult

    echo "$TAR_FILE sent to $ADDRESS"
fi

#unpack archive
ssh $ADDRESS "tar -xzf $TAR_FILE"

if [ $TAR_FILE_EXISTS_REMOTE -ne 0 ]; then
    # copy archive into distribution cache directory
    ssh $ADDRESS "mv $TAR_FILE $ROOT_DIRECTORY/distributionCache"
fi

#npm install http
ssh $ADDRESS "source .bash_profile; cd $HTTP_DIRECTORY; npm install"
checkResult

#npm install deployment
ssh $ADDRESS "source .bash_profile; cd $DEPLOYMENT_DIRECTORY; npm install"
checkResult